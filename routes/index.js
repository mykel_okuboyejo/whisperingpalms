var express = require('express');
var redis = require('redis');
var router = express.Router();

//***************************** Keys ******************************


/* GET home page. */
router.get('/', function(req, res, next) {
	client.set('track','Beautiful Exchange');
	client.get('track', function(err, reply) {
    console.log(reply);
})
  res.render('index', { title: 'Express' });
});

router.get('/dashboard', function(req, res, next) {
  res.render('dashboard', { title: 'Daily Progress' });
});

/**
* Endpoint to receive Events from Core to be stored in the cache and procesed
*/
router.get('/newuser', function(req, res, next) {
	//get previous user count, increment and update
	//push to pusher
  	res.status(200).json({status : true, message : client.get('track')})
});

/**
* Endpoint to receive Events from Core to be stored in the cache and procesed
*/
router.get('/newpayinguser', function(req, res, next) {
  res.status(200).json({status : true, message : "Event Handled"})
});


/**
* Endpoint to receive Events from Core to be stored in the cache and procesed
*/
router.get('/newtransaction', function(req, res, next) {
  res.status(200).json({status : true, message : "Event Handled"})
});

module.exports = router;
